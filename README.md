# README (Not exactly a README style)
It was noticed that most terminal-emulators in linux does not have a support for
Unicode Character Set Level 2 implementation as per ISO 10646. Due to this
most terminal emulators have trouble printing characters in languages like malayalam.
The difficulty is in printing the combining characters which make up the vowel diacritics
and consonant conjunctions etc.. Even though the characters can be printed in
terminals supporting UTF-8 which also works well with input methods like xim, uim, ibus etc..
But the combining characters doesn't just work. But they do work in
KDE's Konsole and GNOME Terminal. 

To get malayalam properly working on this terminal sounded really hard and my initial
attempt failed. But malayalam works so flawlessly on web browsers so just as work 
around to use malayalam in terminal, this weber intends to build a web browser based
terminal emulator. Type in commands like usual but in a webpage served by weber. 
This will then run in background using bash or zsh as per configuration.
This is not by any means a terminal for common usage but just intended for 
working in malayalam on a terminal without heavier counterparts like KDE or GNOME.
